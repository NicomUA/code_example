const path = require('path');
const nunjucks = require('nunjucks');
const env = process.env.NODE_ENV || 'development';
const isDev = process.env.NODE_ENV !== 'production' || process.env.NODE_ENV === '';
const winston = require('winston');
const bodyParser = require('body-parser');
const cors = require('cors');

module.exports = (app)=> {
  app.use(bodyParser.urlencoded());
  app.use(bodyParser.json());
  app.use(cors());

  //settings for rendering pages
  app.set('views', path.join(__dirname,'../views'));
  app.set('view engine', 'html');
  nunjucks.configure('', {
    autoescape: true,
    express: app,
    noCache: isDev
  });

  const config = require('./app')[env];
  app.CONFIG = config;

  //Logger
  app.logger = winston.createLogger({
    level: 'info',
    format: winston.format.json(),
    transports: [
      new winston.transports.File({ filename: app.CONFIG.logger.dir + 'error.log', level: 'error' }),
      new winston.transports.File({ filename: app.CONFIG.logger.dir + 'all.log' })
    ]
  });

  if (isDev) {
    app.use(cors({origin: '*'}));
    app.CONFIG.isDev = isDev;

    app.logger.add(new winston.transports.Console({
      format: winston.format.simple()
    }));
  }

  return app;
};
