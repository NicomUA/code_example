const path = require('path');

module.exports = {
  development: {
    SERVER: {
      PORT: 8000
    },
    DB: {
      url: 'mongodb://localhost/db_1',
      connectionOptions: { useMongoClient: true }
    },
    logger: {
      dir: path.join(__dirname,'../logs/')
    }
  },
  production: {
    SERVER: {
      PORT: 8000
    },
    DB: {
      url: 'mongodb://localhost/db_1',
      connectionOptions: { useMongoClient: true }
    },
    logger: {
      dir: path.join(__dirname,'../logs/')
    }
  }
};
