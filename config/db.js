const mongoose = require('mongoose');
let db = mongoose.connection;
mongoose.Promise = global.Promise; // ES6 Promise inject to Mongoose

module.exports = (app)=> {
  mongoose.connect(app.CONFIG.DB.url, app.CONFIG.DB.connectionOptions);
  if(app.CONFIG.isDev) {
    mongoose.set('debug', true);
  }

  db.on('error', console.error.bind(console, 'connection error:'));
  return db;
};
