const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const TaskSchema = require('./task.model');

const Folder = mongoose.Schema({
  name: String,
  order: Number,

  //ref to prev folder
  prev: {
    type: Schema.Types.ObjectId,
    ref: 'Folder',
    default: null
  },

  //ref to next folder
  next: {
    type: Schema.Types.ObjectId,
    ref: 'Folder',
    default: null
  },
  tasks: [TaskSchema],

  // date fields
  created: {
    type: Date,
    default: Date.now()
  },
  updated: {
    type: Date,
    default: Date.now()
  }
}, { usePushEach: true });

Folder.pre('find', function () {
  this.populate('tasks');
});


//TODo: add board ref

module.exports = mongoose.model('Folder', Folder);