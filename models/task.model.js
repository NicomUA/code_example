const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Task = mongoose.Schema({
  title: String,
  description: String,
  locked: {
    type: Boolean,
    default: false
  },
  order: { type: Number, default: 99},

  // ref to holder 
  folder: {
    type: Schema.Types.ObjectId,
    ref: 'Folder'
  },

  // date fields
  created: {
    type: Date,
    default: Date.now()
  },
  updated: {
    type: Date,
    default: Date.now()
  }
});

module.exports = Task;