const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const UserSchema = require('./user');

const Board = mongoose.Schema({
  name: String,
  created: { type: Date, default: Date.now() },
  updated: { type: Date, default: Date.now() },
});

//TODo: add owners and sharing

module.exports = mongoose.model('Board', Board);
