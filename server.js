const express = require('express');

//server setup
const app = require('./config/server')(express());
const server = require('http').createServer(app);
const db = require('./config/db')(app);

const FoldersModel = require('./models/folder.model');

// websocket setup
const io = require('socket.io')(server);
io.on('connection', (client) => {
  FoldersModel.find().exec((err, items) => {
    client.emit('data:folders', items);
  });

  // add folders handlers
  require('./routes/socket/folders.socket')(io, client);
  require('./routes/socket/tasks.socket')(io, client);
});



//static 
app.use('/static', express.static('static'));

// basic route
app.use('/', require('./routes/index'));


//nothing founded 404 route
app.use(function (req, res, next) {
  res.status(404).render('views/error.html');
});


let PORT = app.CONFIG.SERVER.PORT || 3000;

server.listen(PORT, () => {
  console.log(`--== ${process.env.NODE_ENV} ==-- `);
  console.log(`Server start at port ${PORT}`);
});