import { Component, OnInit, Input, SimpleChange } from '@angular/core';
import { FoldersService } from '../services/folders.service';
import { TaskService } from '../services/task.service';
import { SocketIo } from 'ng-io';
import { MatDialog } from '@angular/material';
import { TaskEditorComponent } from '../task-editor/task-editor.component';
import * as _ from "lodash";

import { DragulaService } from 'ng2-dragula/ng2-dragula';

@Component({
  selector: 'folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.css']
})
export class FolderComponent implements OnInit  {
  @Input() folder;
  ngOnInit() {};
  differ: any;
  folderBinded : boolean = false;
  
  constructor(
    private $folder: FoldersService, 
    private $task: TaskService,
    private dialog: MatDialog) {
    $folder.updated().subscribe((updatedData: any) => {
      if(updatedData._id === this.folder._id) {
        this.folder = updatedData;
      }
    });

    $task.updated().subscribe((taskData: any) => {
      let taskIndex = _.findIndex(this.folder.tasks, (task :any) => task._id == taskData._id);
      if (taskIndex != -1){
        this.folder.tasks[taskIndex] = taskData;
      }
    });
  }

  deleteFolder(id) {
    this.$folder.delete(id);
  }

  isTaskLocked(id){
    let task = _.find(this.folder.tasks, (task)=> task._id == id);
    return task.locked;
  }

  addTask() {
    let dialogRef = this.dialog.open(TaskEditorComponent, {
      width: '600px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result === null) return;

      this.$task.add(result.title, result.description, this.folder._id);
    });
  }
}
