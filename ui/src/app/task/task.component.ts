import { Component, OnInit, Input, SimpleChange, Output} from '@angular/core';
import { TaskService } from '../services/task.service';
import { TaskEditorComponent } from '../task-editor/task-editor.component';
import { MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent implements OnInit {
  constructor(
    private $task: TaskService, 
    private dialog: MatDialog,
    public snackBar: MatSnackBar) {
  }

  ngOnInit() {}
  @Input() task;
  @Input() folderId;

  showLockWarning(){
    this.snackBar.open('Task is locked', null, { duration: 500 });
  }

  deleteTask(id){
    if (this.task.locked) {
      this.showLockWarning();
      return;
    }

    this.$task.delete(id);
  };

  editTask() {
    if (this.task.locked) {
      this.showLockWarning();
      return;
    }

    this.$task.lock(this.task._id);

    let dialogRef = this.dialog.open(TaskEditorComponent, {
      width: '600px',
      data: {
        task: this.task,
        folder: this.folderId
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        this.$task.update({
          id: this.task._id,
          title: result.title,
          description: result.description
        });
      }
      else{
        this.$task.unlock(this.task._id);
      }
    });
  }
}
