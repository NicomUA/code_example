import { Injectable } from '@angular/core';
import { SocketIo  } from 'ng-io';
import * as _ from "lodash";

@Injectable()
export class FoldersService {
  constructor(private socket: SocketIo) { }
  folders : any = [];

  add(title: string) {
    this.socket.emit("folders:add", title);
  }

  delete(id: string) {
    this.socket.emit("folders:delete", id);
  }

  updateTasks(id, tasksList) {
    this.socket.emit("folders:updateTasks", { id: id, tasks: tasksList});
  }

  updated() {
    return this.socket
      .fromEvent("folder:updated")
      .map(data => data);
  }

  isDropAllowed (from, to) {
    let folder: any = _.find(this.folders, { _id: from});
    return (folder._id == to) || (folder.next == to) || (folder.prev == to);
  }

  getAll() {
    return this.socket
      .fromEvent("data:folders")
      .map(data => data)
      .do((data)=>{
        console.log(data);
        this.folders = data;
      });
  }
}