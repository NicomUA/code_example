import { Injectable } from '@angular/core';
import { SocketIo  } from 'ng-io';

@Injectable()
export class TaskService {
  constructor(private socket: SocketIo) { }

  lockedTasks = [];

  add(title:string, desc:string, folder: string) {
    this.socket.emit('task:add',{
      title: title,
      description: desc,
      folder: folder
    });
  }

  update(taskData) {
    this.socket.emit('task:update', taskData);
  }

  updated() {
    return this.socket
      .fromEvent("task:updated")
      .map(data => data);
  }

  delete(id: string) {
    this.socket.emit('task:delete', id);
  }
  
  updatePosition(id, from, to){
    if (from === to) return;
    this.socket.emit('task:updatePosition', {id:id, from: from, to: to});
  }

  lock(id: string) {
    this.socket.emit('task:lock', id);
  }

  unlock(id: string) {
    this.socket.emit('task:unlock', id);
  }
}