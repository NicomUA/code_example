import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms'
import { DragulaModule } from 'ng2-dragula';
import { MatDialogModule } from '@angular/material';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatInputModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'

import { AppComponent } from './app.component';
import { TaskService } from './services/task.service';
import { FoldersService } from './services/folders.service';

import { NgIoModule, NgIoConfig } from 'ng-io';
import { FolderComponent } from './folder/folder.component';
import { TaskComponent } from './task/task.component';
import { TaskEditorComponent } from './task-editor/task-editor.component';

const config: NgIoConfig = { url: 'http://localhost:8000', options: {} };

@NgModule({
  declarations: [
    AppComponent,
    FolderComponent,
    TaskComponent,
    TaskEditorComponent
  ],
  entryComponents: [TaskEditorComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    DragulaModule,
    MatDialogModule, MatFormFieldModule, MatInputModule, MatSnackBarModule,
    NgIoModule.forRoot(config) 
  ],
  providers: [TaskService, FoldersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
