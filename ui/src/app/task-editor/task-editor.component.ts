import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-task-editor',
  templateUrl: './task-editor.component.html',
  styleUrls: ['./task-editor.component.css']
})
export class TaskEditorComponent implements OnInit {
  task = {
    title: '',
    description: ''
  }
  
  ngOnInit() {}
  constructor(public thisDialogRef: MatDialogRef<TaskEditorComponent>, @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data && data.task) {
      this.task.title = data.task.title;
      this.task.description = data.task.description;
      // task editing can synced by binding task in editor and task from taskComponent
      // this.task = data.task
    }
  }

  onCloseConfirm() {
    this.thisDialogRef.close(this.task);
  }
  onCloseCancel() {
    this.thisDialogRef.close(null);
  }

}
