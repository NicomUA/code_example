import { Component } from '@angular/core';
import { FoldersService } from './services/folders.service';
import { TaskService } from './services/task.service';
import { DragulaService } from 'ng2-dragula';
import { TaskEditorComponent } from './task-editor/task-editor.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  model = {
    message: ''
  };
  folders;
  
  constructor(
    private $folder: FoldersService,
    private $task: TaskService,
    private dragulaService: DragulaService) {
      $folder.getAll().subscribe(response => this.folders = response);
      dragulaService.drop.subscribe((value) => {
        const [bagName, e, target, source] = value;
        // get data from element
        const fromContainer = source.getAttribute('data-folder');
        const toContainer = target.getAttribute('data-folder');
        const taskId = e.getAttribute('data-id');

        $task.updatePosition(taskId, fromContainer, toContainer);
        console.log(`task ${taskId} moved from ${fromContainer} to ${toContainer}`);
      });

      dragulaService.setOptions('tasks-folder', {
        accepts: function (el, target, source, sibling) {
          const fromContainer = source.getAttribute('data-folder');
          const toContainer = target.getAttribute('data-folder');
          const isAllowToDrop = $folder.isDropAllowed(fromContainer, toContainer);

          return isAllowToDrop;
        },
        moves: function (el){
          return el.getAttribute('data-locked') == 'false';
        }
      });

    dragulaService.drag.subscribe((value) => {
      const [bagName, e, el] = value;
      console.log('drag');
      console.log('id is:', e.dataset.id);
      console.log('folder is:', e.dataset.folderId);
    });
  };

  addFolder() {
    this.$folder.add('test');
  }

}
