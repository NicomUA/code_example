const FolderModel = require('../../models/folder.model');
const async = require('async');

module.exports = (io, client) => {
  let sendAllFolders = ()=>{
    FolderModel.find().exec((err, items) => {
      io.sockets.emit('data:folders', items);
    });
  };

  let folderUpdated = (folder) => {
    io.sockets.emit('folder:updated', folder);
  }

  client.on('folders:add', (name) => {
    let folder = new FolderModel({ name: name});

    async.series([
      (cb)=>{
        FolderModel.findOne().sort({_id:-1}).exec((err, item) => {
          if(err) cb(err);
          cb(item);
        });
      }
    ], (lastFolder) => {
      if (lastFolder) {
        folder.prev = lastFolder._id;
      }

      folder.save((err, _folder)=> {
        if (lastFolder) {
          lastFolder.next = _folder._id;
          lastFolder.save((err, _lastFolder)=>{
            sendAllFolders();
          });
        }
        else{
          sendAllFolders();
        }

        io.sockets.emit('folder:added', _folder);
      });
    });
  });

  client.on('folders:updateTasks', (data) => {
    FolderModel.findById(data.id).exec((err, item) => {
      item.tasks = data.tasks;
      item.save((err, folder)=>{
        io.sockets.emit('folder:updated', folder);
      });
    });
  });

  client.on('folders:delete', (id) => {
    FolderModel.findById(id).exec((err, item)=>{
      if (item) {
        if (item.next && item.prev) {
          FolderModel.update({ _id: item.next }, { prev: item.prev }).exec();
          FolderModel.update({ _id: item.prev }, { next: item.next }).exec();
        }
        else {
          if (item.next) {
            FolderModel.update({ _id: item.next},{ prev: null }).exec();
          }
  
          if (item.prev) {
            FolderModel.update({ _id: item.prev},{ next: null }).exec();
          }
        }

        item.remove((err)=>{
          sendAllFolders();
        });
      }
    });
  });

  client.on('folders:update', (data) => {
    FolderModel.updateById(data.id, { title: title }).exec((err, _folder)=>{
      io.sockets.emit('folder:updated', _folder);
    })
  });

}