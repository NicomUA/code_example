const FolderModel = require('../../models/folder.model');
const async = require('async');
const _ = require('lodash');

module.exports = (io, client) => {
  let folderUpdated = (folder)=>{
    io.sockets.emit('folder:updated', folder);
  }

  let taskUpdated = (task) => {
    io.sockets.emit('task:updated', task);
  }

  let sendAllFolders = () => {
    FolderModel.find().exec((err, items) => {
      io.sockets.emit('data:folders', items);
    });
  };

  let updateTaskLock = (id, isLocked)=>{
    FolderModel.findOne({ 'tasks._id': id }, 'tasks').exec((err, folder) => {
      if (err) throw new Error(err);

      let task = _.find(folder.tasks, (t) => {
        return (t._id == id);
      });

      task.locked = isLocked;
      folder.save();

      taskUpdated(task);
    });
  }

  client.on('task:add', (data) => {
    let task = data;
    FolderModel.findById(task.folder).exec((err, folder)=>{
      folder.tasks.push(task);
      folder.save((err, _folder)=>{
        folderUpdated(_folder);
      });
    });

  });

  client.on('task:lock', (id) => {
    updateTaskLock(id, true);
  });

  client.on('task:unlock', (id) => {
    updateTaskLock(id, false);
  });

  client.on('task:delete', (id) => {
    FolderModel.findOne({'tasks._id':id}).exec((err, folder)=>{
      folder.tasks.pull({_id:id});
      folder.save((err, _folder) => {
        folderUpdated(_folder);
      });
    });
  });

  client.on('task:updatePosition',(data) => {
    FolderModel.findOne({ 'tasks._id': data.id }, 'tasks').exec((err, folder) => {
      if (err) throw new Error(err);

      let task = _.find(folder.tasks, (t) => {
        return (t._id == data.id);
      });

      task.folder = data.to;

      if (task) {
        folder.tasks.pull(task);
        async.parallel([
          (cb)=>{
            FolderModel.update({_id: data.to},{$push:{tasks: task}}).exec(()=>cb());
          },
          (cb)=>{
            folder.save(()=>cb())
          }
        ],()=>{
          sendAllFolders();
        });
      }
    });

  });

  client.on('task:update', (taskData) => {
    FolderModel.findOne({ 'tasks._id': taskData.id }, 'tasks').exec((err, folder) => {
      if (err) throw new Error(err);
      
      let task = _.find(folder.tasks, (t) => {
        return t._id == taskData.id;
      });

      task.title = taskData.title;
      task.description = taskData.description;
      task.locked = false;
      task.updated = Date.now();

      folder.save();

      taskUpdated(task);
    });
  });

}