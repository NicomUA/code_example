const router = require('express').Router();
const _ = require('lodash');
const path = require('path');

const env = process.env.NODE_ENV || 'development';
const CONFIG = require('../config/app')[env];

router.get('/', (req, resp) => {
  resp.sendFile(path.join(__dirname , '../static/app/index.html'));
});

module.exports = router;
